/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamosui.util;

import com.google.common.io.Resources;
import java.io.File;
import javax.swing.JLabel;

/**
 *
 * @author gamos
 */
public class Util {
    public static String previousFile = null;
    public static void showFileDialogAndSetPath(JLabel label) {
        File file = FileHelper.openFileBrowser(previousFile);
        previousFile = file.getAbsolutePath();
        if (file != null) {
            label.setText(file.getAbsolutePath());
        }
    }

    public static String previousDir = null;
    public static void showDirDialogAndSetPath(JLabel label) {
        File file = FileHelper.openDirBrowser(previousDir);
        previousDir = file.getAbsolutePath();
        if (file != null) {
            label.setText(file.getAbsolutePath());
        }
    }
    
    public static String appPath() {
        String resPath = Resources.getResource("resources/no").getPath().replace("file:", "");
        File file = new File(resPath);
        String resPathDebug = "/build/classes/resources/no";
        if(resPath.contains(resPathDebug)) {
            return resPath.replace(resPathDebug, "");
        } else {
            return resPath.replace(File.separator + "resources/no", "").replace(File.separator + "GamosUI.jar!", "");
        }
    }
}
