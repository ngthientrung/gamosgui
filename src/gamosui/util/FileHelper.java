/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamosui.util;

import java.io.File;
import javax.swing.JFileChooser;

/**
 *
 * @author gamos
 */
public class FileHelper {

    static String desktopPath() {
        String desktopPath = System.getProperty("user.home") + "/Desktop";
        return desktopPath.replace("\\", "/");
    }

    static File openFileBrowser(String file) {
        String openFile = file == null ? desktopPath() : file;
        File path = new File(openFile);
        JFileChooser jfc = new JFileChooser(path);
        int returnValue = jfc.showOpenDialog(null);

        if (returnValue == JFileChooser.APPROVE_OPTION) {
            File f = jfc.getSelectedFile();
            return f;
        }
        return null;
    }

    static File openDirBrowser(String file) {
        String openFile = file == null ? desktopPath() : file;
        File path = new File(openFile);
        JFileChooser jfc = new JFileChooser(path);
        jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnValue = jfc.showOpenDialog(null);

        if (returnValue == JFileChooser.APPROVE_OPTION) {
            return jfc.getSelectedFile();
        }
        return null;
    }
}
