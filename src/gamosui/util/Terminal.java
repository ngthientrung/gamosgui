/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamosui.util;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import org.zeroturnaround.exec.InvalidExitValueException;
import org.zeroturnaround.exec.ProcessExecutor;
import org.zeroturnaround.exec.stream.LogOutputStream;

/**
 *
 * @author gamos
 */
public class Terminal {

    public static class Commands {

        ArrayList<String> lines = new ArrayList<>();

        public void add(String template, Object... args) {
            lines.add(String.format(template + ";", args));
        }

    }

    private String[] makeCommandlines(Commands commands) {
        ArrayList<String> list = new ArrayList<>();
        list.add("/bin/bash");
        list.add("-c");
        String singleComment = "";
        for (String l : commands.lines) {
            singleComment += l;
        }
        list.add(singleComment);
        return list.toArray(new String[list.size()]);
    }

    class MainProcess implements Runnable {

        final Commands commands;
        final JTextArea textArea;
        final Process process;

        public MainProcess(Commands commands, JTextArea textArea, Process process) {
            this.commands = commands;
            this.textArea = textArea;
            this.process = process;
        }

        @Override
        public void run() {
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    SwingUtilities.invokeLater(new PrintText(textArea, line));
                    Thread.sleep(10);
                }
            } catch (IOException ex) {
                Logger.getLogger(Terminal.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(Terminal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    class PrintText implements Runnable {

        final JTextArea textArea;
        final String line;

        public PrintText(JTextArea textArea, String line) {
            this.textArea = textArea;
            this.line = line;
        }

        @Override
        public void run() {
            textArea.append(line + "\n");
        }

    }

    @SuppressWarnings("empty-statement")
    public void exec2(Commands commands, JTextArea textArea) {

        try {
            OutputStream outputStream = new LogOutputStream() {
                @Override
                protected void processLine(String string) {
                    new Thread(() -> {
                        textArea.append(string + "\n");
                    }).start();
                }
            };
            textArea.setText("");
            String[] cmd = makeCommandlines(commands);
            ProcessExecutor executor = (new ProcessExecutor())
                    .command(cmd)
                    .redirectOutput(outputStream);

            new Thread(() -> {
                try {
                    executor.execute();
                } catch (IOException | InterruptedException | TimeoutException | InvalidExitValueException ex) {
                    Logger.getLogger(Terminal.class.getName()).log(Level.SEVERE, null, ex);
                }
            }).start();;

//        ProcessBuilder processBuilder = new ProcessBuilder();
//        
//        processBuilder.command(cmd);
//        InputStream inputStream = processBuilder.start().getInputStream();
//        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
//
//        String line;
//        try {
//            while ((line = reader.readLine()) != null) {
//                SwingUtilities.invokeLater(new PrintText(textArea, line));
//            }
//        } catch (IOException ex) {
//            Logger.getLogger(Terminal.class.getName()).log(Level.SEVERE, null, ex);
//        }
        } catch (InvalidExitValueException ex) {
            Logger.getLogger(Terminal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void exec(Commands commands) throws IOException, InterruptedException {
        Runtime rt = Runtime.getRuntime();
        String[] cmd = makeCommandlines(commands);

        Process process = rt.exec(cmd);
        int exitCode = process.waitFor();
        System.out.println("\nExited with error code : " + exitCode);
    }
}
